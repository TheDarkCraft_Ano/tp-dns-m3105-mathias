#!/usr/bin/env bash

## configuration de wiki.org 
# configuration du serveur dns

himage dwikiorg mkdir -p /etc/named
hcp wiki.org/named.conf dwikiorg:/etc/.
hcp wiki.org/* dwikiorg:/etc/named/.


himage dwikiorg rm /etc/named/named.conf 


## configuration de iut.re 
# configuration du serveur dns

himage diutre mkdir -p /etc/named
hcp iut.re/named.conf diutre:/etc/.
hcp iut.re/* diutre:/etc/named/.
himage diutre rm /etc/named/named.conf 


## configuration de pc1
# resolv.conf

hcp pc1/resolv.conf pc1:/etc/.

## configuration de dorg
# named.conf

himage dorg mkdir -p /etc/named
hcp dorg/named.conf dorg:/etc/.
hcp dorg/* dorg:/etc/named/.
himage dorg rm /etc/named/named.conf

## configuration de dre
# named.conf

himage dre mkdir -p /etc/named
hcp dre/named.conf dre:/etc/.
hcp dre/* dre:/etc/named/.
himage dre rm /etc/named/named.conf

## configuration de dwikiorg
# named.conf + all in named/

himage dwikiorg mkdir -p /etc/named
hcp wiki.org/named.conf dwikiorg:/etc/.
hcp wiki.org/* dwikiorg:/etc/named/.
himage dwikiorg rm /etc/named/named.conf

## configuration aRootServer

himage aRootServer mkdir -p /etc/named
hcp aroot/named.conf aRootServer:/etc/.
hcp aroot/* aRootServer:/etc/named/.
himage aRootServer rm /etc/named/named.conf




